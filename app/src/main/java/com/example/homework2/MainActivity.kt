package com.example.homework2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        openSecondActivity.setOnClickListener {
            openSecondActivity()
        }
    }

    private fun openSecondActivity(){
        val intent=Intent(this,SecondActivity::class.java)
        val userModel= UserModel(firstnameTextView.text.toString(),lastnameTextView.text.toString(),emailTextView.text.toString(),birthdateTextView.text.toString().toInt(),genderTextView.text.toString())

        intent.putExtra("userModel",userModel)
        startActivityForResult(intent, REQUEST_CODE)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode==Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val userModel=data!!.getParcelableExtra("userModel") as UserModel
            d("UpdateInfo********", userModel.toString())
            firstnameTextView.text=userModel.firstname
            lastnameTextView.text=userModel.lastname
            emailTextView.text=userModel.email
            birthdateTextView.text= userModel.birthdate.toString()
            genderTextView.text=userModel.gender
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


}
