package com.example.homework2

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init(){
        val userModel:UserModel = intent.getParcelableExtra("userModel") as UserModel

        firstnameEditText.setText(userModel.firstname)
        lastnameEditText.setText((userModel.lastname))
        emailEditText.setText(userModel.email)
        birthdateEditText.setText((userModel.birthdate.toString()))
        genderEditText.setText(userModel.gender)

        returnMainActivity.setOnClickListener {
            returnMainActivity()

        }
    }

    private fun returnMainActivity() {
        val userModel= UserModel(firstnameEditText.text.toString(),lastnameEditText.text.toString(),emailEditText.text.toString(),birthdateEditText.text.toString().toInt(),genderEditText.text.toString())
        val intent = intent
        intent.putExtra("userModel",userModel)
        setResult(Activity.RESULT_OK, intent)
        finish()


    }


    }

